
function $args(func) {
    return (func + '')
        .replace(/[/][/].*$/mg,'') // strip single-line comments
        .replace(/\s+/g, '') // strip white space
        .replace(/[/][*][^/*]*[*][/]/g, '') // strip multi-line comments
        .split('){', 1)[0].replace(/^[^(]*[(]/, '') // extract the parameters
        .replace(/=[^,]+/g, '') // strip any ES6 defaults
        .split(',').filter(Boolean); // split & filter [""]
}
module.exports = function(lib, socket, options){
    let opts = Object.assign({
        lib,
        socket,
        route:lib

    },options);

    let library = require(opts.lib);
    if(opts.libProp){
        library = library[opts.libProp];
    }
    let libInfo = {};
    Object.getOwnPropertyNames(library).filter(item => typeof library[item] === 'function').forEach(name=>{
        libInfo[name] = {parameters : $args(library[name])};



    });

    socket.on(opts.route,(req)=>{
        console.log('received for',opts.route,req)
        let clientTo = req.from;
        let clientCmdId = req.cmdId;
        try{
            let method =  req.method;
            if(libInfo[method]){
                if(req.evalBefore){
                    eval(req.evalBefore);
                }
                library[method](...req.params).then(
                    function(){
                        let response = {
                            to:clientTo,
                            cmdId:clientCmdId,
                            result:[...arguments],
                            errors:null
                        }

                        if(req.evalResult){
                            response.afterEval = eval(req.evalResult);
                        }
                        console.log(response);
                        socket.emit(opts.route,response)
                    }
                ).catch(function(){
                    let response = {
                        to:clientTo,
                        cmdId:clientCmdId,
                        errors:[...arguments],
                        result:null
                    };
                    console.log(response);
                    socket.emit(opts.route,response);
                })
            }else{

                let response = {
                    to:clientTo,
                    cmdId:clientCmdId,
                    errors:'no method ' + method,
                    result:null
                };
                console.log(response);
                socket.emit(opts.route,response);
            }
        }catch (e) {
            let response = {
                to:clientTo,
                cmdId:clientCmdId,
                errors:e.toString(),
                result:null
            };
            console.log(response);
            socket.emit(opts.route,response);
        }
    })

    //console.log(libInfo);

}